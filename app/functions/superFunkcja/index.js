exports.handler = (event, context, callback) => {
  let value = values(event.guid);
  console.log('guid', event.guid);
  if (!value) {
    callback('Guid not found', null);
    return;
  }
  callback(null, value);
};


function values(guid) {
  let data = {
    a6bbd544e6ebe7609c53b6049fa2e406dc5fd802: {
      "CampaignHistory": [
        {"name": "Zzzz", "visitedAt": "aaaa", "medium": "asd"},
        {"name": "Zzzz", "visitedAt": "aaaa", "medium": "asd"}
      ],
      "Sessions": 301,
      "PageViews": 666,
      "VehicleHistory": [
        {"id": 151365, "type": "Car", "manufacturer": "Ford"},
        {"id": 151365, "type": "Car", "manufacturer": "Ford"},
        {"id": 151365, "type": "Car", "manufacturer": "Ford"}
      ]
    },
    a6bbd544e6ebe7609c53b6049fa2e406123: {
      "CampaignHistory": [
        {"name": "Zzzz", "visitedAt": "aaaa", "medium": "asd"},
        {"name": "Zzzz", "visitedAt": "aaaa", "medium": "asd"}
      ],
      "Sessions": 301,
      "PageViews": 666,
      "VehicleHistory": [
        {"id": 151365, "type": "Car", "manufacturer": "Ford"},
        {"id": 151365, "type": "Car", "manufacturer": "Ford"},
        {"id": 151365, "type": "Car", "manufacturer": "Ford"}
      ]
    },
  };
  return data[guid];
}