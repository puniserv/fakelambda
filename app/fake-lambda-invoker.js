const express = require('express');
const http = require('http');
const app = express();
const fs = require('fs');
const parse = require('url-parse');
const host = process.env.FAKEMAIL_HOST;
const server = http.Server(app);
const functionsDir = __dirname + '/functions/';
app.use(function(req, res, next){
   let data = "";
   req.on('data', function(chunk){ data += chunk});
   req.on('end', function(){
       req.rawBody = data;
       req.jsonBody = JSON.parse(data);
       next();
   });
});

app.post('*', function (req, res) {  
  let originalUrl = req.originalUrl;
  let headers = req.headers;
  let urlParts = parse(originalUrl, true);
  let pathParts = urlParts.pathname.split('/');
  if(pathParts[1]!== '2015-03-31') {
    res.send({errorMessage:'Invalid version. Only 2015-03-31 supported'});
    return;
  }
  if(pathParts[2]!== 'functions') {
    res.send({errorMessage:'Invalid url (functions)'});
    return;
  }
  if(pathParts[4]!== 'invocations') {
    res.send({errorMessage: 'Invalid url (invocations)'});
    return;
  }
  let fileName = pathParts[3];
  let functionDir = functionsDir + fileName;
  if (!fs.existsSync(functionDir)) {
    let error = functionDir + ' not found';
    res.send({errorMessage: error});
    return;
  }
  new Promise((resolve) => {
    const lambda = require(functionDir + '/index.js');
    lambda.handler(req.jsonBody, {headers: headers}, function (error, success) {
      if(error) {
        resolve({errorMessage: error});
        return;
      }
      resolve(success);
    });
  }).then(function (data) {
    res.send(data);
  });
});
server.listen(80);
