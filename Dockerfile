FROM node:9.5.0
RUN npm i -g nodemon
WORKDIR /app
COPY app/package.json package.json
COPY app/fake-lambda-invoker.js fake-lambda-invoker.js
COPY app/web web
RUN npm install
ENTRYPOINT ["nodemon"]
CMD ["-L", "fake-lambda-invoker.js", "0.0.0.0"]
EXPOSE 80
